﻿
using System;
using System.Windows;


namespace Media_Player_Paulo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        { 
            InitializeComponent();
            frame.Content = new Lecteur();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            frame.Navigate(new Uri("Lecteur.xaml", UriKind.Relative));  
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            frame.Navigate(new Uri("Radio.xaml", UriKind.Relative));
        }
    }
}

