﻿using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Threading;


namespace Media_Player_Paulo
{
    /// <summary>
    /// Logique d'interaction pour Lecteur.xaml
    /// </summary>
    public partial class Lecteur : Page
    {
        public  MediaPlayer player = new MediaPlayer();
        private string[] files;
        private string[] path;
        public DispatcherTimer timer = new DispatcherTimer();


        public Lecteur()
        {
            InitializeComponent();
        }

        private void UpdateProgressBar(object sender, EventArgs e)
        {
            if (player.Source != null)
            {
                prog.Minimum = 0;
                if (player.NaturalDuration.HasTimeSpan)
                {
                    prog.Maximum = player.NaturalDuration.TimeSpan.TotalSeconds;
                    prog.Value = player.Position.TotalSeconds;
                } 
            }
        }
        
        private void OpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "MP3 files(*.mp3)|*.mp3|All files(*.*)|*.*";
            openFileDialog.InitialDirectory = @"D:\MP3\";
            

            if (openFileDialog.ShowDialog() == true)
            {
                files = openFileDialog.SafeFileNames;
                path = openFileDialog.FileNames;
                listMp3.Items.Clear();

                foreach (string song in path)
                {
                    timer.Interval = TimeSpan.FromSeconds(1);
                    timer.Tick += UpdateProgressBar;

                    if (!listMp3.Items.Contains(song))
                    {
                        ListBoxItem item = new ListBoxItem();
                        for (int i = 0; i <= listMp3.Items.Count; i++)
                        {
                            item.Content = files[i];
                        }
                        listMp3.Items.Add(item);
                    }
                }
            }  
        }
        
        private void ListMp3_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                player.Open(new Uri(path[listMp3.SelectedIndex]));
                player.Play();
                timer.Start();
            }
            catch
            {
               
            }          
        }

        private void Play_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (path!=null)
                {
                    player.Open(new Uri(path[listMp3.SelectedIndex]));
                    player.Play();
                    timer.Start();
                }
                else
                {
                    MessageBox.Show("Veuillez ouvrir une playlist", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                       
            }
            catch
            {
                MessageBox.Show("Selectionnez un mp3 dans la liste", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (path != null)
                {
                    if (listMp3.SelectedIndex != listMp3.Items.Count - 1)
                    {
                        listMp3.SelectedIndex += 1;
                    }
                    player.Open(new Uri(path[listMp3.SelectedIndex]));
                    player.Play();
                    timer.Start();
                }
                else
                {
                    MessageBox.Show("Veuillez ouvrir une playlist", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch
            {

            }   
        }

        private void Previous_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (path != null)
                {
                    if (listMp3.SelectedIndex > 0)
                    {
                        listMp3.SelectedIndex -= 1;
                    }

                    player.Open(new Uri(path[listMp3.SelectedIndex]));
                    player.Play();
                    timer.Start();
                }
                else
                {
                    MessageBox.Show("Veuillez ouvrir une playlist", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch
            {

            }  
        }

        bool isPaused = false;
        private void Pause_Click(object sender, RoutedEventArgs e)
        {
            if (isPaused == false)
            {
                player.Pause();
                isPaused = true;
            }
            else
            {
                player.Play();
                isPaused = false;
            }
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            player.Stop();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            player.Volume = vol.Value / 100; 
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigating += NavigationService_Navigating;
        }

        private void NavigationService_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            player.Stop();
            NavigationService.Navigating -= NavigationService_Navigating;
        }
    }
}
