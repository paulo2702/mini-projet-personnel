﻿using RadioBrowser;
using RadioBrowser.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Navigation;

namespace Media_Player_Paulo
{
   
    /// <summary>
    /// Logique d'interaction pour Radio.xaml
    /// </summary>
    public partial class Radio : Page
    {
        public MediaPlayer player = new MediaPlayer();
        private readonly List<Uri> radioUrls = new List<Uri>();
        private readonly List<string> searchNames = new List<string>();
       

        public Radio()
        {
            InitializeComponent();
            InitialyseClient();
           
           
        }

        public async void InitialyseClient()
        {
            
            RadioBrowserClient radioBrowser = new RadioBrowserClient();
            var advancedSearch = await radioBrowser.Search.AdvancedAsync(new AdvancedSearchOptions
            {
                Countrycode = "fr", 
            });

            foreach (var station in advancedSearch)
            {
                if (!listRadio.Items.Contains(station))
                {
                    searchNames.Add(station.Name);
                    radioUrls.Add(station.Url);
                    
                    ListBoxItem itemName = new ListBoxItem();
                    foreach (string name in searchNames)
                    {
                        itemName.Content = name;    
                    }
                    listRadio.ItemsSource=searchNames;
                    CollectionView view = CollectionViewSource.GetDefaultView(listRadio.ItemsSource) as CollectionView;
                    view.Filter = CustomFilter;
                }
            }
            
        }

        private void ListRadio_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
                player.Open(new Uri(radioUrls[listRadio.SelectedIndex].ToString()));
                player.Play();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            player.Volume = vol.Value / 100;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigating += NavigationService_Navigating;
        }

        private void NavigationService_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            player.Stop();
            NavigationService.Navigating -= NavigationService_Navigating;
        }

        private bool CustomFilter(object obj)
        {
            if (string.IsNullOrEmpty(textsearch.Text))
            {
                return true;
            }
            else
            {
                return obj.ToString().IndexOf(textsearch.Text, StringComparison.OrdinalIgnoreCase) >= 0;
            }
        }

        private void Textsearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(listRadio.ItemsSource).Refresh();
        }
    }
}
