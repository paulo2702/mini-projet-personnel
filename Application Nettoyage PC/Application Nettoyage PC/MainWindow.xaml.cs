﻿using System;
using System.Linq;
using System.Windows;
using System.Diagnostics;
using System.IO;
using System.Net;


namespace Application_Nettoyage_PC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string version = "1.0.0";
        public DirectoryInfo winTemp;
        public DirectoryInfo appTemp;
        
       

        public MainWindow()
        {
            InitializeComponent();
            winTemp = new DirectoryInfo(@"C:\Windows\Temp");
            appTemp = new DirectoryInfo(System.IO.Path.GetTempPath());
            //CheckActu();
            //CheckVersion();
            GetDate();
        }

        /// <summary>
        /// Verifier les actus
        /// </summary>
        public void CheckActu()
        {

            string url = "http://localhost:8000/actus.txt";

            using (WebClient client = new WebClient())
            {
                string actu = client.DownloadString(url);
                if (actu != String.Empty)
                {
                    actuTxt.Content = actu;
                    actuTxt.Visibility = Visibility.Visible;
                    bandeau.Visibility = Visibility.Visible;
                }
            }
        }

        /// <summary>
        /// Verifier les versions
        /// </summary>
        public void CheckVersion()
        {
            try
            {
                string url = "http://localhost:8000/version.txt";
                using (WebClient client = new WebClient())
                {
                    string v = client.DownloadString(url);
                    if (version != v)
                    {
                        MessageBox.Show("Une mise à jour est dispo !", "Mise à jour", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Votre logiciel est à jour !", "Mise à jour", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Le serveur web est indisponible !", "Mise à jour", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        /// <summary>
        /// Calculer la taille d'un dossier
        /// </summary>
        /// <param name="dir">Dossier à traiter</param>
        /// <returns>Taille totale des fichiers à supprimer</returns>
        public long DirSize(DirectoryInfo dir)
        {
            return dir.GetFiles().Sum(fi => fi.Length) + dir.GetDirectories().Sum(di => DirSize(di));
        }

        /// <summary>
        /// Vider les dossiers
        /// </summary>
        /// <param name="di">Dossier à supprimer</param>
        public void ClearTempData(DirectoryInfo di)
        {
            foreach (FileInfo file in di.GetFiles())
            {
                try
                {
                    file.Delete();
                    Console.WriteLine(file.FullName);
                }
                catch (Exception)
                {
                    continue;
                }
            }

            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                try
                {
                    dir.Delete(true);
                    Console.WriteLine(dir.FullName);
                }
                catch (Exception)
                {
                    continue;
                }
            }
        }


        private void Button_Clean_Click(object sender, RoutedEventArgs e)
        {
            int nbNettoyage = 0;
            nbNettoyage++;




            Console.WriteLine("Nettoyage en cours...");
            btnClean.Content = "Nettoyage en cours...";

            Clipboard.Clear();


            try
            {
                ClearTempData(winTemp);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur : " + ex.Message);
            }

            try
            {
                ClearTempData(appTemp);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur : " + ex.Message);
            }


            title.Content = "Nettoyage effectué !";
            space.Content = "0 Mb";
            btnClean.Content = "Nettoyer";
            SaveNbNettoyage(nbNettoyage);

        }

        private void Button_History_Click(object sender, RoutedEventArgs e)
        {

            Window2 win2 = new Window2();
            win2.Show();

        }

        private void Button_Web_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(new ProcessStartInfo("http://www.jphemery.fr")
                {
                    UseShellExecute = true
                });
            }
            catch (Exception exception)
            {
                Console.WriteLine("Erreur :" + exception.Message);
            }

        }

        private void Button_MAJ_Click(object sender, RoutedEventArgs e)
        {
            CheckVersion();
        }

        private void Button_Analyser_Click(object sender, RoutedEventArgs e)
        {

            AnalyseFolders();

        }
        /// <summary>
        /// Analyser les dossiers
        /// </summary>
        public void AnalyseFolders()
        {
            int nbAnalyse = 0;
            nbAnalyse++;



            Console.WriteLine("Début de l'analyse...");
            long totalSize = 0;




            try
            {
                long totalEspace = long.Parse(File.ReadAllText("espace.txt"));
                totalSize += DirSize(winTemp) / 1000000;
                totalSize += DirSize(appTemp) / 1000000;
                totalEspace += totalSize;
                String totalEsp = totalEspace.ToString();
                File.WriteAllText("espace.txt", totalEsp);
            }

            catch (Exception ex)
            {
                Console.WriteLine("Impossible d'analyser les dossiers : " + ex.Message);
            }

            space.Content = totalSize + " Mb";
            title.Content = "Analyse effectuée !";
            date.Content = String.Format("{0:dd/MM/yyyy}", DateTime.Today);

            SaveNbAnalyse(nbAnalyse);
            SaveDate();





        }
        /// <summary>
        /// Sauvegarde date dernière analyse
        /// </summary>
        public void SaveDate()
        {
            string date= String.Format("{0:dd/MM/yyyy}", DateTime.Today);
            File.WriteAllText("date.txt", date);
        }

        /// <summary>
        /// Affichage dernière analyse
        /// </summary>
        public void GetDate()
        {
            string dateFichier = File.ReadAllText("date.txt");

            if (dateFichier != String.Empty)
            {
                date.Content = dateFichier;
            }
        }

        /// <summary>
        /// Sauvegarde nombre d'analyses
        /// </summary>
        /// <param name="nbAnalyse"></param>
        /// 
        public void SaveNbAnalyse(int nbAnalyse)
        {
            string totalNbAnalyse = File.ReadAllText("analyse.txt");

            string totalA = (nbAnalyse + int.Parse(totalNbAnalyse)).ToString();

            File.WriteAllText("analyse.txt", totalA);

        }

        /// <summary>
        /// Sauvegarde nombre de nettoyages
        /// </summary>
        /// <param name="nbNettoyage"></param>
        public void SaveNbNettoyage(int nbNettoyage)
        {
            string totalNbNettoyage = File.ReadAllText("nettoyage.txt");

            string totalN = (nbNettoyage + int.Parse(totalNbNettoyage)).ToString();

            File.WriteAllText("nettoyage.txt", totalN);

        }

    }

}