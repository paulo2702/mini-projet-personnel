﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Application_Nettoyage_PC
{
    /// <summary>
    /// Logique d'interaction pour Window2.xaml
    /// </summary>
    public partial class Window2 : Window
    {
        public Window2()
        {
            InitializeComponent();
            string totalNbAnalyse = File.ReadAllText("analyse.txt");
            string totalNbNettoyage = File.ReadAllText("nettoyage.txt");
            string totalTailleNettoyé = File.ReadAllText("espace.txt");
            analyse.Content = totalNbAnalyse + " analyses depuis l'installation du logiciel";
            nettoyage.Content = totalNbNettoyage + " nettoyages depuis l'installation du logiciel";
            taille.Content = totalTailleNettoyé + " Mb" + " de fichiers temporaires.";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            fenetre2.Close();
        }
    }
}