let villeChoisie;
if ("geolocation" in navigator) {
    navigator.geolocation.watchPosition((position) => {


        const url = 'https://api.openweathermap.org/data/2.5/weather?lon=' + position.coords.longitude +
            '&lat=' + position.coords.latitude + '&appid=0b040e843fa8ad3d40c9ba173173a6d8&units=metric';


        fetch(url)
            .then(function (response) {
                return response.json()
            }) // Convert data to json
            .then(function (response) {
                //console.log(response);
                let temperature = response.main.temp;
                document.querySelector('#temperature_label').textContent = temperature;
                let ville = response.name;
                document.querySelector('#ville').textContent = ville;
            })
            .catch(function () {
                // catch any errors
                alert('Un problême est survenu, veuillez revenir plus tard !');
            });


    }, error, options);
} else {
    villeChoisie = "Paris";
    recupererTemperature(villeChoisie);
}

var options = {
    enableHighAccuracy: true
}

let changerDeVille = document.querySelector('#changer');

changerDeVille.addEventListener('click', () => {
    villeChoisie = prompt('Quelle ville choisissez vous?');
    recupererTemperature(villeChoisie);
})

function error() {
    villeChoisie = "Paris";
    recupererTemperature(villeChoisie);
}

function recupererTemperature(ville) {
    const url = 'https://api.openweathermap.org/data/2.5/weather?q=' + ville + '&appid=0b040e843fa8ad3d40c9ba173173a6d8&units=metric';

    fetch(url)
        .then(function (response) {
            return response.json()
        }) // Convert data to json
        .then(function (response) {
            //console.log(response);
            let temperature = response.main.temp;
            document.querySelector('#temperature_label').textContent = temperature;
            let ville = response.name;
            document.querySelector('#ville').textContent = ville;
        })
        .catch(function () {
            // catch any errors
            alert('Un problême est survenu, veuillez revenir plus tard !');
        });
}